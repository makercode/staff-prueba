
//jQuery global
global.jQuery= require('../vendor/jquery/jquery-3.3.1.min');
//jQuery plugins
var $owl = require('../vendor/owl/owl.carousel.min');
//Fire functions
(function($) {
  //Log Fire
  console.log( "rdy4pty" );
  //Owl Fire
  $('.js-carousel-team').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
    nav: false,
    responsive:{
        320:{
            items:1,
            nav:true
        },
        480:{
            items:1,
            nav:false
        },
        768:{
            items:2,
            nav:true,
            loop:false
        },
        992:{
            items:3,
            nav:true,
            loop:false
        },
        1200:{
            items:3,
            nav:true,
            loop:false
        }
    }
  });
  //Basic menú
  $( "nav" ).on( "click", '.js-btn-menu', function() {
    $(".main-menu").toggleClass('active');
  });
  
})( jQuery );
